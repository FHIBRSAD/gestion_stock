package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.Categorie;

public interface ICategorieService {
	public Categorie save(Categorie entity);
	public Categorie update(Categorie entity);
	public List<Categorie>	selectAll();
	public List<Categorie>	selectAll(String sortfield, String sort);
	public Categorie getById(long id);
	public void remove(long id);
	public Categorie findOne(String paramName, Object paramValue);
	public Categorie findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName, String paramValue);

}

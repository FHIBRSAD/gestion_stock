package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.LigneCdeClient;

public interface ILigneCdeClientService {
	public LigneCdeClient save(LigneCdeClient entity);
	public LigneCdeClient update(LigneCdeClient entity);
	public List<LigneCdeClient>	selectAll();
	public List<LigneCdeClient>	selectAll(String sortfield, String sort);
	public LigneCdeClient getById(long id);
	public void remove(long id);
	public LigneCdeClient findOne(String paramName, Object paramValue);
	public LigneCdeClient findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName, String paramValue);

}

package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IMvtStkDao;
import com.stock.mvc.entities.MvtStk;
import com.stock.mvc.services.IMvtStckService;

@Transactional
public class MvtStckServiceImpl implements IMvtStckService{

	private IMvtStkDao MvtStkDao;
	public void setMvtStkDao(IMvtStkDao mvtStkDao) {
		MvtStkDao = mvtStkDao;
	}
	
	@Override
	public MvtStk save(MvtStk entity) {
		return MvtStkDao.save(entity);
	}

	@Override
	public MvtStk update(MvtStk entity) {
		return MvtStkDao.update(entity);
	}

	@Override
	public List<MvtStk> selectAll() {
		return MvtStkDao.selectAll();
	}

	@Override
	public List<MvtStk> selectAll(String sortfield, String sort) {
		return MvtStkDao.selectAll(sortfield, sort);
	}

	@Override
	public MvtStk getById(long id) {
		return MvtStkDao.getById(id);
	}

	@Override
	public void remove(long id) {
		MvtStkDao.remove(id);		
	}

	@Override
	public MvtStk findOne(String paramName, Object paramValue) {
		return MvtStkDao.findOne(paramName, paramValue);
	}

	@Override
	public MvtStk findOne(String[] paramNames, Object[] paramValues) {
		return MvtStkDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return MvtStkDao.findCountBy(paramName, paramValue);
	}

}

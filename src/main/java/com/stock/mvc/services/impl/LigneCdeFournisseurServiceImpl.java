package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneCdeFournisseurDao;
import com.stock.mvc.entities.LigneCdeFournisseur;
import com.stock.mvc.services.ILigneCdeFournisseurService;

@Transactional
public class LigneCdeFournisseurServiceImpl implements ILigneCdeFournisseurService {

	private ILigneCdeFournisseurDao LigneCdeFournisseurDao;
	public void setLigneCdeFournisseurDao(ILigneCdeFournisseurDao ligneCdeFournisseurDao) {
		LigneCdeFournisseurDao = ligneCdeFournisseurDao;
	}
	
	@Override
	public LigneCdeFournisseur save(LigneCdeFournisseur entity) {
		return LigneCdeFournisseurDao.save(entity);
	}

	@Override
	public LigneCdeFournisseur update(LigneCdeFournisseur entity) {
		return LigneCdeFournisseurDao.update(entity);
	}

	@Override
	public List<LigneCdeFournisseur> selectAll() {
		return LigneCdeFournisseurDao.selectAll();
	}

	@Override
	public List<LigneCdeFournisseur> selectAll(String sortfield, String sort) {
		return LigneCdeFournisseurDao.selectAll(sortfield, sort);
	}

	@Override
	public LigneCdeFournisseur getById(long id) {
		return LigneCdeFournisseurDao.getById(id);
	}

	@Override
	public void remove(long id) {
		LigneCdeFournisseurDao.remove(id);		
	}

	@Override
	public LigneCdeFournisseur findOne(String paramName, Object paramValue) {
		return LigneCdeFournisseurDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCdeFournisseur findOne(String[] paramNames, Object[] paramValues) {
		return LigneCdeFournisseurDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return LigneCdeFournisseurDao.findCountBy(paramName, paramValue);
	}

}

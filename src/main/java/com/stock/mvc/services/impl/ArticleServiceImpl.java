package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IArticleDao;
import com.stock.mvc.entities.Article;
import com.stock.mvc.services.IArticleService;

@Transactional
public class ArticleServiceImpl implements IArticleService {

	private IArticleDao Dao;
	
	public void setDao(IArticleDao dao) {
		Dao = dao;
	}
	@Override
	public Article save(Article entity) {
		return Dao.save(entity);
	}

	@Override
	public Article update(Article entity) {
		return Dao.update(entity);
	}

	@Override
	public List<Article> selectAll() {
		return Dao.selectAll();
	}

	@Override
	public List<Article> selectAll(String sortfield, String sort) {
		return Dao.selectAll(sortfield, sort);
	}

	@Override
	public Article getById(long id) {
		return Dao.getById(id);
	}

	@Override
	public void remove(long id) {
		Dao.remove(id);
	}

	@Override
	public Article findOne(String paramName, Object paramValue) {
		return Dao.findOne(paramName, paramValue);
	}

	@Override
	public Article findOne(String[] paramNames, Object[] paramValues) {
		return Dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return Dao.findCountBy(paramName, paramValue);
	}

}

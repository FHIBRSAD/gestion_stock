package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ICategorieDao;
import com.stock.mvc.entities.Categorie;
import com.stock.mvc.services.ICategorieService;

@Transactional
public class CategorieServiceImpl implements ICategorieService {

	private ICategorieDao CategorieDao;
	
	public void setCategorieDao(ICategorieDao categorieDao) {
		CategorieDao = categorieDao;
	}
	@Override
	public Categorie save(Categorie entity) {
		return CategorieDao.save(entity);
	}

	@Override
	public Categorie update(Categorie entity) {
		return CategorieDao.update(entity);
	}

	@Override
	public List<Categorie> selectAll() {
		return CategorieDao.selectAll();
	}

	@Override
	public List<Categorie> selectAll(String sortfield, String sort) {
		return CategorieDao.selectAll(sortfield, sort);
	}

	@Override
	public Categorie getById(long id) {
		return CategorieDao.getById(id);
	}

	@Override
	public void remove(long id) {
		CategorieDao.remove(id);		
	}

	@Override
	public Categorie findOne(String paramName, Object paramValue) {
		return CategorieDao.findOne(paramName, paramValue);
	}

	@Override
	public Categorie findOne(String[] paramNames, Object[] paramValues) {
		return CategorieDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return CategorieDao.findCountBy(paramName, paramValue);
	}

}

package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IUtilisateurDao;
import com.stock.mvc.entities.Utilisateur;
import com.stock.mvc.services.IUtilisateurService;

@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService{

	private IUtilisateurDao UtilisateurDao;
	public void setUtilisateurDao(IUtilisateurDao utilisateurDao) {
		UtilisateurDao = utilisateurDao;
	}
	@Override
	public Utilisateur save(Utilisateur entity) {
		return UtilisateurDao.save(entity);
	}

	@Override
	public Utilisateur update(Utilisateur entity) {
		return UtilisateurDao.update(entity);
	}

	@Override
	public List<Utilisateur> selectAll() {
		return UtilisateurDao.selectAll();
	}

	@Override
	public List<Utilisateur> selectAll(String sortfield, String sort) {
		return UtilisateurDao.selectAll(sortfield, sort);
	}

	@Override
	public Utilisateur getById(long id) {
		return UtilisateurDao.getById(id);
	}

	@Override
	public void remove(long id) {
		UtilisateurDao.remove(id);		
	}

	@Override
	public Utilisateur findOne(String paramName, Object paramValue) {
		return UtilisateurDao.findOne(paramName, paramValue);
	}

	@Override
	public Utilisateur findOne(String[] paramNames, Object[] paramValues) {
		return UtilisateurDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return UtilisateurDao.findCountBy(paramName, paramValue);
	}

}

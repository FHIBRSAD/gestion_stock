package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IClientDao;
import com.stock.mvc.entities.Client;
import com.stock.mvc.services.IClientService;

@Transactional
public class ClientServiceImpl implements IClientService {

	private IClientDao ClientDao;
	public void setClientDao(IClientDao clientDao) {
		ClientDao = clientDao;
	}
	@Override
	public Client save(Client entity) {
		return ClientDao.save(entity);
	}

	@Override
	public Client update(Client entity) {
		return ClientDao.update(entity);
	}

	@Override
	public List<Client> selectAll() {
		return ClientDao.selectAll();
	}

	@Override
	public List<Client> selectAll(String sortfield, String sort) {
		return ClientDao.selectAll(sortfield, sort);
	}

	@Override
	public Client getById(long id) {
		return ClientDao.getById(id);
	}

	@Override
	public void remove(long id) {
		ClientDao.remove(id);		
	}

	@Override
	public Client findOne(String paramName, Object paramValue) {
		return ClientDao.findOne(paramName, paramValue);
	}

	@Override
	public Client findOne(String[] paramNames, Object[] paramValues) {
		return ClientDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return ClientDao.findCountBy(paramName, paramValue);
	}

}

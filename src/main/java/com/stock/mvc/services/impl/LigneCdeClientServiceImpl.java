package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneCdeClientDao;
import com.stock.mvc.entities.LigneCdeClient;
import com.stock.mvc.services.ILigneCdeClientService;

@Transactional
public class LigneCdeClientServiceImpl implements ILigneCdeClientService{

	private ILigneCdeClientDao LigneCdeClientDao;
	public void setLigneCdeClientDao(ILigneCdeClientDao ligneCdeClientDao) {
		LigneCdeClientDao = ligneCdeClientDao;
	}
	@Override
	public LigneCdeClient save(LigneCdeClient entity) {
		return LigneCdeClientDao.save(entity);
	}

	@Override
	public LigneCdeClient update(LigneCdeClient entity) {
		return LigneCdeClientDao.update(entity);
	}

	@Override
	public List<LigneCdeClient> selectAll() {
		return LigneCdeClientDao.selectAll();
	}

	@Override
	public List<LigneCdeClient> selectAll(String sortfield, String sort) {
		return LigneCdeClientDao.selectAll();
	}

	@Override
	public LigneCdeClient getById(long id) {
		return LigneCdeClientDao.getById(id);
	}

	@Override
	public void remove(long id) {
		LigneCdeClientDao.remove(id);		
	}

	@Override
	public LigneCdeClient findOne(String paramName, Object paramValue) {
		return LigneCdeClientDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCdeClient findOne(String[] paramNames, Object[] paramValues) {
		return LigneCdeClientDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return LigneCdeClientDao.findCountBy(paramName, paramValue);
	}

}

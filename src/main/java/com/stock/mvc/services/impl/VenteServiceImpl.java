package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IVenteDao;
import com.stock.mvc.entities.Vente;
import com.stock.mvc.services.IVenteService;

@Transactional
public class VenteServiceImpl implements IVenteService{

	private IVenteDao VenteDao;
	public void setVenteDao(IVenteDao venteDao) {
		VenteDao = venteDao;
	}
	@Override
	public Vente save(Vente entity) {
		return VenteDao.save(entity);
	}

	@Override
	public Vente update(Vente entity) {
		return VenteDao.update(entity);
	}

	@Override
	public List<Vente> selectAll() {
		return VenteDao.selectAll();
	}

	@Override
	public List<Vente> selectAll(String sortfield, String sort) {
		return VenteDao.selectAll(sortfield, sort);
	}

	@Override
	public Vente getById(long id) {
		return VenteDao.getById(id);
	}

	@Override
	public void remove(long id) {
		VenteDao.remove(id);		
	}

	@Override
	public Vente findOne(String paramName, Object paramValue) {
		return VenteDao.findOne(paramName, paramValue);
	}

	@Override
	public Vente findOne(String[] paramNames, Object[] paramValues) {
		return VenteDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return VenteDao.findCountBy(paramName, paramValue);
	}


}

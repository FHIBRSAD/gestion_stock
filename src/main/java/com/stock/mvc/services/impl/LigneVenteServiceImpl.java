package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneVenteDao;
import com.stock.mvc.entities.LigneVente;
import com.stock.mvc.services.ILigneVenteService;

@Transactional
public class LigneVenteServiceImpl implements ILigneVenteService{

	private ILigneVenteDao LigneVenteDao;
	public void setLigneVenteDao(ILigneVenteDao ligneVenteDao) {
		LigneVenteDao = ligneVenteDao;
	}
	
	@Override
	public LigneVente save(LigneVente entity) {
		return LigneVenteDao.save(entity);
	}

	@Override
	public LigneVente update(LigneVente entity) {
		return LigneVenteDao.update(entity);
	}

	@Override
	public List<LigneVente> selectAll() {
		return LigneVenteDao.selectAll();
	}

	@Override
	public List<LigneVente> selectAll(String sortfield, String sort) {
		return LigneVenteDao.selectAll(sortfield, sort);
	}

	@Override
	public LigneVente getById(long id) {
		return LigneVenteDao.getById(id);
	}

	@Override
	public void remove(long id) {
		LigneVenteDao.remove(id);		
	}

	@Override
	public LigneVente findOne(String paramName, Object paramValue) {
		return LigneVenteDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneVente findOne(String[] paramNames, Object[] paramValues) {
		return LigneVenteDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return LigneVenteDao.findCountBy(paramName, paramValue);
	}

}

package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ICommandeFournisseurDao;
import com.stock.mvc.entities.CommandeFournisseur;
import com.stock.mvc.services.ICommandeFournisseurService;

@Transactional
public class CommandeFournisseurServiceImpl implements ICommandeFournisseurService {

	private ICommandeFournisseurDao CommandeFournisseurDao;
	public void setCommandeFournisseurDao(ICommandeFournisseurDao commandeFournisseurDao) {
		CommandeFournisseurDao = commandeFournisseurDao;
	}
	
	@Override
	public CommandeFournisseur save(CommandeFournisseur entity) {
		return CommandeFournisseurDao.save(entity);
	}

	@Override
	public CommandeFournisseur update(CommandeFournisseur entity) {
		return CommandeFournisseurDao.update(entity);
	}

	@Override
	public List<CommandeFournisseur> selectAll() {
		return CommandeFournisseurDao.selectAll();
	}

	@Override
	public List<CommandeFournisseur> selectAll(String sortfield, String sort) {
		return CommandeFournisseurDao.selectAll(sortfield, sort);
	}

	@Override
	public CommandeFournisseur getById(long id) {
		return CommandeFournisseurDao.getById(id);
	}

	@Override
	public void remove(long id) {
		CommandeFournisseurDao.remove(id);		
	}

	@Override
	public CommandeFournisseur findOne(String paramName, Object paramValue) {
		return CommandeFournisseurDao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
		return CommandeFournisseurDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return CommandeFournisseurDao.findCountBy(paramName, paramValue);
	}

}

package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ICommandeClientDao;
import com.stock.mvc.entities.CommandeClient;
import com.stock.mvc.services.ICommandeClientService;

@Transactional
public class CommandeClientServiceImpl implements ICommandeClientService {

	private ICommandeClientDao CommandeClientDao;
	public void setCommandeClientDao(ICommandeClientDao commandeClientDao) {
		CommandeClientDao = commandeClientDao;
	}
	
	@Override
	public CommandeClient save(CommandeClient entity) {
		return CommandeClientDao.save(entity);
	}

	@Override
	public CommandeClient update(CommandeClient entity) {
		return CommandeClientDao.update(entity);
	}

	@Override
	public List<CommandeClient> selectAll() {
		return CommandeClientDao.selectAll();
	}

	@Override
	public List<CommandeClient> selectAll(String sortfield, String sort) {
		return CommandeClientDao.selectAll(sortfield, sort);
	}

	@Override
	public CommandeClient getById(long id) {
		return CommandeClientDao.getById(id);
	}

	@Override
	public void remove(long id) {
		CommandeClientDao.remove(id);		
	}

	@Override
	public CommandeClient findOne(String paramName, Object paramValue) {
		return CommandeClientDao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeClient findOne(String[] paramNames, Object[] paramValues) {
		return CommandeClientDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return CommandeClientDao.findCountBy(paramName, paramValue);
	}

}

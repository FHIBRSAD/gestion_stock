package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.LigneCdeFournisseur;

public interface ILigneCdeFournisseurService {
	public LigneCdeFournisseur save(LigneCdeFournisseur entity);
	public LigneCdeFournisseur update(LigneCdeFournisseur entity);
	public List<LigneCdeFournisseur>	selectAll();
	public List<LigneCdeFournisseur>	selectAll(String sortfield, String sort);
	public LigneCdeFournisseur getById(long id);
	public void remove(long id);
	public LigneCdeFournisseur findOne(String paramName, Object paramValue);
	public LigneCdeFournisseur findOne(String[] paramNames, Object[] paramValues);
	public int findCountBy(String paramName, String paramValue);

}

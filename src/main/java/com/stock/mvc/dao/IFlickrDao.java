package com.stock.mvc.dao;

import java.io.InputStream;

public interface IFlickrDao {
	public String savePhoto(InputStream stream, String filename) throws Exception;
}

package com.stock.mvc.dao;

import com.stock.mvc.entities.LigneCdeClient;

public interface ILigneCdeClientDao extends IGenericDao<LigneCdeClient> {

}

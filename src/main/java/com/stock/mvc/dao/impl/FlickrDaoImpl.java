package com.stock.mvc.dao.impl;

import java.io.InputStream;

import javax.swing.JOptionPane;

import org.scribe.model.Verifier;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.stock.mvc.dao.IFlickrDao;

import 	org.scribe.model.Token;

public class FlickrDaoImpl implements IFlickrDao {

	private Flickr flickr;
	private UploadMetaData uploadMetaData = new UploadMetaData();
	private String apiKey = "212b53b9f4077d5593f4c6cadfc0f8bd";
	private String sharedSecret= "9f99d050ab07dd92";

	private void connect(){
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157703337624145-dad418715262b57f");
		auth.setTokenSecret("d6adfedc70b956a5");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);

	}

	@Override
	public String savePhoto(InputStream photo, String title) throws Exception{
		connect();
		uploadMetaData.setTitle(title);
		String photoId = flickr.getUploader().upload(photo,uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();	
	}

	public void auth(){
		flickr = new Flickr(apiKey,sharedSecret, new REST());
		AuthInterface authInterface = flickr.getAuthInterface();
		Token token = authInterface.getRequestToken();
		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println ("Url access");
		System.out.println (url);
		System.out.println ("Copier Coller Token");
		System.out.println (">>");
		
		String tokenKey = JOptionPane.showInputDialog(null);
		Token requestToken = authInterface.getAccessToken(token, new Verifier(tokenKey));
		System.out.println ("Auth sucess");
		
		Auth auth=null;
		try{
			auth = authInterface.checkToken(requestToken);
		}catch (FlickrException e){
			e.printStackTrace();
		}
		//This token can be used until user revokes it
		System.out.println ("Token: " + requestToken.getToken());
		System.out.println ("Secret: " + requestToken.getSecret());
		System.out.println ("nsid: " + auth.getUser().getId());
		System.out.println ("Realname: " + auth.getUser().getRealName());
		System.out.println ("Username: " + auth.getUser().getUsername());
		System.out.println ("Permission: " + auth.getPermission().getType());
	}
}

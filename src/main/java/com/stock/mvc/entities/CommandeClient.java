package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeClient implements Serializable {

	@Id
	@GeneratedValue
	private long idCommandeClient;
	
	@OneToMany (mappedBy ="commandeClient")
	private List<LigneCdeClient> ligneCdeClient;
	
	public List<LigneCdeClient> getLigneCdeClient() {
		return ligneCdeClient;
	}

	public void setLigneCdeClient(List<LigneCdeClient> ligneCdeClient) {
		this.ligneCdeClient = ligneCdeClient;
	}

	private String code;
	@Temporal (TemporalType.TIMESTAMP)
	private Date dateCommande;
	
	@ManyToOne
	@JoinColumn(name="idClient")
	private Client client;
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	
	public long getIdCommandeClient() {
		return idCommandeClient;
	}

	public void setIdCommandeClient(long id) {
		this.idCommandeClient = id;
	}
}

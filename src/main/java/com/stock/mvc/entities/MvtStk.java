package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvtStk implements Serializable {

    public static final int ENTREE=1;
    public static final int SORTIE=2;
	
	@Id
	@GeneratedValue
	private long idMvtStck;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article articleMvtStck;
	
	private String code;
	@Temporal (TemporalType.TIMESTAMP)
	private Date dateMvtStck;

	private BigDecimal quantite;
	
	private int typeMvt;
	
	public Article getArticleMvtStck() {
		return articleMvtStck;
	}

	public void setArticleMvtStck(Article articleMvtStck) {
		this.articleMvtStck = articleMvtStck;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateMvtStck() {
		return dateMvtStck;
	}

	public void setDateMvtStck(Date dateMvtStck) {
		this.dateMvtStck = dateMvtStck;
	}

	public long getIdMvtStck() {
		return idMvtStck;
	}

	public void setIdMvtStck(long id) {
		this.idMvtStck = id;
	}
}

package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
public class LigneCdeFournisseur implements Serializable {

	@Id
	@GeneratedValue
	private long idLigneCommandeFournisseur;

	@ManyToOne
	@JoinColumn(name="idCommandeFournisseur")
	private CommandeFournisseur commandeFournisseur;
	
	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

	public Article getArticleFournisseur() {
		return articleFournisseur;
	}

	public void setArticleFournisseur(Article articleFournisseur) {
		this.articleFournisseur = articleFournisseur;
	}

	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article articleFournisseur;
	
	public long getIdLigneCommandeFournisseur() {
		return idLigneCommandeFournisseur;
	}

	public void setIdLigneCommandeFournisseur(long id) {
		this.idLigneCommandeFournisseur = id;
	}
}

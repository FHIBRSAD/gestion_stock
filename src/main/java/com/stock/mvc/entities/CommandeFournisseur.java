package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeFournisseur implements Serializable {

	@Id
	@GeneratedValue
	private long idCommandeFournisseur;

	@ManyToOne
	@JoinColumn(name="idFournisseur")
	private Fournisseur fournisseur;
	
	@OneToMany (mappedBy ="commandeFournisseur")
	private List<LigneCdeFournisseur> lignecommandeFournisseur;
	
	
	
	public List<LigneCdeFournisseur> getLignecommandeFournisseur() {
		return lignecommandeFournisseur;
	}

	public void setLignecommandeFournisseur(List<LigneCdeFournisseur> lignecommandeFournisseur) {
		this.lignecommandeFournisseur = lignecommandeFournisseur;
	}

	private String code;
	@Temporal (TemporalType.TIMESTAMP)
	private Date dateCommandeFournisseur;
	
	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateCommandeFournisseur() {
		return dateCommandeFournisseur;
	}

	public void setDateCommandeFournisseur(Date dateCommandeFournisseur) {
		this.dateCommandeFournisseur = dateCommandeFournisseur;
	}

	public long getIdCommandeFournisseur() {
		return idCommandeFournisseur;
	}

	public void setIdCommandeFournisseur(long id) {
		this.idCommandeFournisseur = id;
	}
}

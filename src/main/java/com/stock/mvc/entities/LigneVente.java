package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
public class LigneVente implements Serializable {

	@Id
	@GeneratedValue
	private long idLigneVente;
	
	@ManyToOne
	@JoinColumn(name="idVente")
	private Vente vente;
	
	public Vente getVente() {
		return vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}

	public Article getArticleVente() {
		return articleVente;
	}

	public void setArticleVente(Article articleVente) {
		this.articleVente = articleVente;
	}

	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article articleVente;

	public long getIdLigneVente() {
		return idLigneVente;
	}

	public void setIdLigneVente(long id) {
		this.idLigneVente = id;
	}
}

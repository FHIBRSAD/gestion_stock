package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="Article")
public class Article implements Serializable {

	@Id
	@GeneratedValue
	private long idArticle;
	private String codeArticle;
	private String designation;
	private BigDecimal prixUnitaireHT;
	private BigDecimal tauxTva;
	private BigDecimal prixUnitaireTTC;
	private String photo;
	
	@ManyToOne
	@JoinColumn(name="idCategory")
	private Categorie categorie;
	
	public List<LigneCdeClient> getLigneCdeClients() {
		return ligneCdeClients;
	}

	public void setLigneCdeClients(List<LigneCdeClient> ligneCdeClients) {
		this.ligneCdeClients = ligneCdeClients;
	}

	public List<LigneCdeFournisseur> getLignecommandeFournisseur() {
		return lignecommandeFournisseur;
	}

	public void setLignecommandeFournisseur(List<LigneCdeFournisseur> lignecommandeFournisseur) {
		this.lignecommandeFournisseur = lignecommandeFournisseur;
	}

	public List<LigneVente> getLigneVente() {
		return ligneVente;
	}

	public void setLigneVente(List<LigneVente> ligneVente) {
		this.ligneVente = ligneVente;
	}

	public List<MvtStk> getMvtstck() {
		return mvtstck;
	}

	public void setMvtstck(List<MvtStk> mvtstck) {
		this.mvtstck = mvtstck;
	}

	@OneToMany (mappedBy ="article")
	private List<LigneCdeClient> ligneCdeClients;

	@OneToMany (mappedBy ="articleFournisseur")
	private List<LigneCdeFournisseur> lignecommandeFournisseur;
	
	@OneToMany (mappedBy ="articleVente")
	private List<LigneVente> ligneVente;
	
	@OneToMany (mappedBy ="articleMvtStck")
	private List<MvtStk> mvtstck;
	
	public Article() {
		// TODO Auto-generated constructor stub
	}
	
	public long getIdArticle() {
		return idArticle;
	}

	public String getCodeArticle() {
		return codeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixUnitaireHT() {
		return prixUnitaireHT;
	}

	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		this.prixUnitaireHT = prixUnitaireHT;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixUnitaireTTC() {
		return prixUnitaireTTC;
	}

	public void setPrixUnitaireTTC(BigDecimal prixUnitaireTTC) {
		this.prixUnitaireTTC = prixUnitaireTTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public void setIdArticle(long id) {
		this.idArticle = id;
	}
}
